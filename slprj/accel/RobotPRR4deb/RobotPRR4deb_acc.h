#include "__cf_RobotPRR4deb.h"
#ifndef RTW_HEADER_RobotPRR4deb_acc_h_
#define RTW_HEADER_RobotPRR4deb_acc_h_
#include <stddef.h>
#include <float.h>
#ifndef RobotPRR4deb_acc_COMMON_INCLUDES_
#define RobotPRR4deb_acc_COMMON_INCLUDES_
#include <stdlib.h>
#define S_FUNCTION_NAME simulink_only_sfcn 
#define S_FUNCTION_LEVEL 2
#define RTW_GENERATED_S_FUNCTION
#include "rtwtypes.h"
#include "simstruc.h"
#include "fixedpoint.h"
#endif
#include "RobotPRR4deb_acc_types.h"
#include "multiword_types.h"
#include "mwmathutil.h"
#include "rt_defines.h"
#include "rt_nonfinite.h"
typedef struct { real_T mv4jwi0p02 [ 6 ] ; real_T mtmulhmo2u [ 4 ] ; real_T
mql0uu2ddi [ 4 ] ; real_T l33ugzb3ek [ 3 ] ; real_T nbjh4a2wkb [ 4 ] ; real_T
fve1ezraz5 [ 4 ] ; real_T mdmeqplsik [ 4 ] ; real_T l2qlxafopz [ 6 ] ; real_T
aisxq4lwwk ; real_T c5znxqafno [ 14 ] ; real_T ennvnxnrlj [ 6 ] ; real_T
h2bg4jirut [ 4 ] ; real_T l3qcvl2zr1 [ 6 ] ; real_T o0abxadlac [ 4 ] ; real_T
du0zzedao5 [ 3 ] ; real_T d3aqkyzusi [ 3 ] ; real_T fc2wirrpvx [ 4 ] ; real_T
ccai0rx3vm [ 4 ] ; real_T ounvrgf3rq [ 12 ] ; real_T h3bb52snzc [ 12 ] ;
real_T ete2axxtwo ; real_T layys5wvhk [ 4 ] ; } cuhhlll434 ; typedef struct {
struct { real_T modelTStart ; } aptiltq03a ; struct { real_T modelTStart ; }
pgbrn5p0ae ; struct { real_T modelTStart ; } a5isgspqvs ; struct { void *
TUbufferPtrs [ 12 ] ; } g5hbf24wxm ; void * knsjxlwzrc ; struct { void *
TUbufferPtrs [ 8 ] ; } c1refq2331 ; void * ld0t2fnwxb ; struct { void *
TUbufferPtrs [ 8 ] ; } g5n0vcnuuu ; void * ayuliluxlq ; void * dc21el5huo [ 3
] ; void * fwlwrlfya3 [ 3 ] ; void * ppibxtzyci [ 3 ] ; struct { void *
LoggedData ; } g3ltiy4ubc ; void * hy2heumws4 [ 3 ] ; struct { void *
LoggedData ; } hr2s2odivp ; struct { void * LoggedData ; } hmqyqx2hbk ; void
* bi0rdxlwbo ; struct { void * LoggedData ; } ik3izuxeap ; struct { void *
LoggedData ; } fby4tic4qp ; void * pfqkunv2x1 [ 3 ] ; struct { void *
LoggedData ; } bsadq0r2v3 ; struct { void * LoggedData ; } eqmm55ddsc ;
struct { void * LoggedData ; } f5bolscohg ; struct { void * LoggedData ; }
kvfym2v5ok ; struct { void * LoggedData ; } j25ap3vdtn ; struct { void *
LoggedData ; } kfvld3pmdj ; struct { void * LoggedData ; } g3cudvtnau ;
struct { int_T Tail [ 6 ] ; int_T Head [ 6 ] ; int_T Last [ 6 ] ; int_T
CircularBufSize [ 6 ] ; int_T MaxNewBufSize ; } cb55khv33w ; struct { int_T
Tail [ 4 ] ; int_T Head [ 4 ] ; int_T Last [ 4 ] ; int_T CircularBufSize [ 4
] ; int_T MaxNewBufSize ; } mkfvho51fk ; struct { int_T Tail [ 4 ] ; int_T
Head [ 4 ] ; int_T Last [ 4 ] ; int_T CircularBufSize [ 4 ] ; int_T
MaxNewBufSize ; } mzedfxafby ; int_T earveglsw4 ; int_T mjikr3rlw5 ; int_T
asdntwqmru ; int_T npuevwu1kf ; int_T i1yyui0v2a ; } lwbmduhwor ; struct
kdzsm2juuu_ { real_T P_0 ; real_T P_1 [ 6 ] ; real_T P_2 ; real_T P_3 [ 4 ] ;
real_T P_4 ; real_T P_5 [ 4 ] ; real_T P_6 ; real_T P_7 ; real_T P_8 [ 4 ] ;
real_T P_9 ; real_T P_10 [ 6 ] ; real_T P_11 ; real_T P_12 [ 6 ] ; real_T
P_13 [ 4 ] ; } ; extern kdzsm2juuu bzsanxx3ft ;
#endif
