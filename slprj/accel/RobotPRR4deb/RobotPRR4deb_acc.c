#include "__cf_RobotPRR4deb.h"
#include <math.h>
#include "RobotPRR4deb_acc.h"
#include "RobotPRR4deb_acc_private.h"
#include <stdio.h>
#include "simstruc.h"
#include "fixedpoint.h"
#define CodeFormat S-Function
#define AccDefine1 Accelerator_S-Function
#ifndef __RTW_UTFREE__  
extern void * utMalloc ( size_t ) ; extern void utFree ( void * ) ;
#endif
boolean_T RobotPRR4deb_acc_rt_TDelayUpdateTailOrGrowBuf ( int_T * bufSzPtr ,
int_T * tailPtr , int_T * headPtr , int_T * lastPtr , real_T tMinusDelay ,
real_T * * tBufPtr , real_T * * uBufPtr , real_T * * xBufPtr , boolean_T
isfixedbuf , boolean_T istransportdelay , int_T * maxNewBufSzPtr ) { int_T
testIdx ; int_T tail = * tailPtr ; int_T bufSz = * bufSzPtr ; real_T * tBuf =
* tBufPtr ; real_T * xBuf = ( NULL ) ; int_T numBuffer = 2 ; if (
istransportdelay ) { numBuffer = 3 ; xBuf = * xBufPtr ; } testIdx = ( tail <
( bufSz - 1 ) ) ? ( tail + 1 ) : 0 ; if ( ( tMinusDelay <= tBuf [ testIdx ] )
&& ! isfixedbuf ) { int_T j ; real_T * tempT ; real_T * tempU ; real_T *
tempX = ( NULL ) ; real_T * uBuf = * uBufPtr ; int_T newBufSz = bufSz + 1024
; if ( newBufSz > * maxNewBufSzPtr ) { * maxNewBufSzPtr = newBufSz ; } tempU
= ( real_T * ) utMalloc ( numBuffer * newBufSz * sizeof ( real_T ) ) ; if (
tempU == ( NULL ) ) { return ( false ) ; } tempT = tempU + newBufSz ; if (
istransportdelay ) tempX = tempT + newBufSz ; for ( j = tail ; j < bufSz ; j
++ ) { tempT [ j - tail ] = tBuf [ j ] ; tempU [ j - tail ] = uBuf [ j ] ; if
( istransportdelay ) tempX [ j - tail ] = xBuf [ j ] ; } for ( j = 0 ; j <
tail ; j ++ ) { tempT [ j + bufSz - tail ] = tBuf [ j ] ; tempU [ j + bufSz -
tail ] = uBuf [ j ] ; if ( istransportdelay ) tempX [ j + bufSz - tail ] =
xBuf [ j ] ; } if ( * lastPtr > tail ) { * lastPtr -= tail ; } else { *
lastPtr += ( bufSz - tail ) ; } * tailPtr = 0 ; * headPtr = bufSz ; utFree (
uBuf ) ; * bufSzPtr = newBufSz ; * tBufPtr = tempT ; * uBufPtr = tempU ; if (
istransportdelay ) * xBufPtr = tempX ; } else { * tailPtr = testIdx ; }
return ( true ) ; } real_T RobotPRR4deb_acc_rt_TDelayInterpolate ( real_T
tMinusDelay , real_T tStart , real_T * tBuf , real_T * uBuf , int_T bufSz ,
int_T * lastIdx , int_T oldestIdx , int_T newIdx , real_T initOutput ,
boolean_T discrete , boolean_T minorStepAndTAtLastMajorOutput ) { int_T i ;
real_T yout , t1 , t2 , u1 , u2 ; if ( ( newIdx == 0 ) && ( oldestIdx == 0 )
&& ( tMinusDelay > tStart ) ) return initOutput ; if ( tMinusDelay <= tStart
) return initOutput ; if ( ( tMinusDelay <= tBuf [ oldestIdx ] ) ) { if (
discrete ) { return ( uBuf [ oldestIdx ] ) ; } else { int_T tempIdx =
oldestIdx + 1 ; if ( oldestIdx == bufSz - 1 ) tempIdx = 0 ; t1 = tBuf [
oldestIdx ] ; t2 = tBuf [ tempIdx ] ; u1 = uBuf [ oldestIdx ] ; u2 = uBuf [
tempIdx ] ; if ( t2 == t1 ) { if ( tMinusDelay >= t2 ) { yout = u2 ; } else {
yout = u1 ; } } else { real_T f1 = ( t2 - tMinusDelay ) / ( t2 - t1 ) ;
real_T f2 = 1.0 - f1 ; yout = f1 * u1 + f2 * u2 ; } return yout ; } } if (
minorStepAndTAtLastMajorOutput ) { if ( newIdx != 0 ) { if ( * lastIdx ==
newIdx ) { ( * lastIdx ) -- ; } newIdx -- ; } else { if ( * lastIdx == newIdx
) { * lastIdx = bufSz - 1 ; } newIdx = bufSz - 1 ; } } i = * lastIdx ; if (
tBuf [ i ] < tMinusDelay ) { while ( tBuf [ i ] < tMinusDelay ) { if ( i ==
newIdx ) break ; i = ( i < ( bufSz - 1 ) ) ? ( i + 1 ) : 0 ; } } else { while
( tBuf [ i ] >= tMinusDelay ) { i = ( i > 0 ) ? i - 1 : ( bufSz - 1 ) ; } i =
( i < ( bufSz - 1 ) ) ? ( i + 1 ) : 0 ; } * lastIdx = i ; if ( discrete ) {
double tempEps = ( DBL_EPSILON ) * 128.0 ; double localEps = tempEps *
muDoubleScalarAbs ( tBuf [ i ] ) ; if ( tempEps > localEps ) { localEps =
tempEps ; } localEps = localEps / 2.0 ; if ( tMinusDelay >= ( tBuf [ i ] -
localEps ) ) { yout = uBuf [ i ] ; } else { if ( i == 0 ) { yout = uBuf [
bufSz - 1 ] ; } else { yout = uBuf [ i - 1 ] ; } } } else { if ( i == 0 ) {
t1 = tBuf [ bufSz - 1 ] ; u1 = uBuf [ bufSz - 1 ] ; } else { t1 = tBuf [ i -
1 ] ; u1 = uBuf [ i - 1 ] ; } t2 = tBuf [ i ] ; u2 = uBuf [ i ] ; if ( t2 ==
t1 ) { if ( tMinusDelay >= t2 ) { yout = u2 ; } else { yout = u1 ; } } else {
real_T f1 = ( t2 - tMinusDelay ) / ( t2 - t1 ) ; real_T f2 = 1.0 - f1 ; yout
= f1 * u1 + f2 * u2 ; } } return ( yout ) ; } static void mdlOutputs (
SimStruct * S , int_T tid ) { real_T jx5q0qvnjp ; int32_T i ; real_T
h00ezikeyj_idx_0 ; real_T h00ezikeyj_idx_1 ; real_T h00ezikeyj_idx_2 ; real_T
h00ezikeyj_idx_3 ; cuhhlll434 * _rtB ; kdzsm2juuu * _rtP ; lwbmduhwor * _rtDW
; _rtDW = ( ( lwbmduhwor * ) ssGetRootDWork ( S ) ) ; _rtP = ( ( kdzsm2juuu *
) ssGetDefaultParam ( S ) ) ; _rtB = ( ( cuhhlll434 * ) _ssGetBlockIO ( S ) )
; { real_T * * uBuffer = ( real_T * * ) & _rtDW -> g5hbf24wxm . TUbufferPtrs
[ 0 ] ; real_T * * tBuffer = ( real_T * * ) & _rtDW -> g5hbf24wxm .
TUbufferPtrs [ 6 ] ; real_T simTime = ssGetT ( S ) ; real_T tMinusDelay ; {
int_T i1 ; real_T * y0 = & _rtB -> mv4jwi0p02 [ 0 ] ; const real_T * p_P_1 =
_rtP -> P_1 ; int_T * iw_Tail = & _rtDW -> cb55khv33w . Tail [ 0 ] ; int_T *
iw_Head = & _rtDW -> cb55khv33w . Head [ 0 ] ; int_T * iw_Last = & _rtDW ->
cb55khv33w . Last [ 0 ] ; int_T * iw_CircularBufSize = & _rtDW -> cb55khv33w
. CircularBufSize [ 0 ] ; for ( i1 = 0 ; i1 < 6 ; i1 ++ ) { tMinusDelay = ( (
_rtP -> P_0 > 0.0 ) ? _rtP -> P_0 : 0.0 ) ; tMinusDelay = simTime -
tMinusDelay ; y0 [ i1 ] = RobotPRR4deb_acc_rt_TDelayInterpolate ( tMinusDelay
, 0.0 , * tBuffer , * uBuffer , iw_CircularBufSize [ i1 ] , & iw_Last [ i1 ]
, iw_Tail [ i1 ] , iw_Head [ i1 ] , p_P_1 [ i1 ] , 0 , ( boolean_T ) (
ssIsMinorTimeStep ( S ) && ( ssGetTimeOfLastOutput ( S ) == ssGetT ( S ) ) )
) ; tBuffer ++ ; uBuffer ++ ; } } } ssCallAccelRunBlock ( S , 2 , 1 ,
SS_CALL_MDL_OUTPUTS ) ; { real_T * * uBuffer = ( real_T * * ) & _rtDW ->
c1refq2331 . TUbufferPtrs [ 0 ] ; real_T * * tBuffer = ( real_T * * ) & _rtDW
-> c1refq2331 . TUbufferPtrs [ 4 ] ; real_T simTime = ssGetT ( S ) ; real_T
tMinusDelay ; { int_T i1 ; real_T * y0 = & _rtB -> mtmulhmo2u [ 0 ] ; const
real_T * p_P_3 = _rtP -> P_3 ; int_T * iw_Tail = & _rtDW -> mkfvho51fk . Tail
[ 0 ] ; int_T * iw_Head = & _rtDW -> mkfvho51fk . Head [ 0 ] ; int_T *
iw_Last = & _rtDW -> mkfvho51fk . Last [ 0 ] ; int_T * iw_CircularBufSize = &
_rtDW -> mkfvho51fk . CircularBufSize [ 0 ] ; for ( i1 = 0 ; i1 < 4 ; i1 ++ )
{ tMinusDelay = ( ( _rtP -> P_2 > 0.0 ) ? _rtP -> P_2 : 0.0 ) ; tMinusDelay =
simTime - tMinusDelay ; y0 [ i1 ] = RobotPRR4deb_acc_rt_TDelayInterpolate (
tMinusDelay , 0.0 , * tBuffer , * uBuffer , iw_CircularBufSize [ i1 ] , &
iw_Last [ i1 ] , iw_Tail [ i1 ] , iw_Head [ i1 ] , p_P_3 [ i1 ] , 0 , (
boolean_T ) ( ssIsMinorTimeStep ( S ) && ( ssGetTimeOfLastOutput ( S ) ==
ssGetT ( S ) ) ) ) ; tBuffer ++ ; uBuffer ++ ; } } } ssCallAccelRunBlock ( S
, 2 , 3 , SS_CALL_MDL_OUTPUTS ) ; { real_T * * uBuffer = ( real_T * * ) &
_rtDW -> g5n0vcnuuu . TUbufferPtrs [ 0 ] ; real_T * * tBuffer = ( real_T * *
) & _rtDW -> g5n0vcnuuu . TUbufferPtrs [ 4 ] ; real_T simTime = ssGetT ( S )
; real_T tMinusDelay ; { int_T i1 ; real_T * y0 = & _rtB -> mql0uu2ddi [ 0 ]
; const real_T * p_P_5 = _rtP -> P_5 ; int_T * iw_Tail = & _rtDW ->
mzedfxafby . Tail [ 0 ] ; int_T * iw_Head = & _rtDW -> mzedfxafby . Head [ 0
] ; int_T * iw_Last = & _rtDW -> mzedfxafby . Last [ 0 ] ; int_T *
iw_CircularBufSize = & _rtDW -> mzedfxafby . CircularBufSize [ 0 ] ; for ( i1
= 0 ; i1 < 4 ; i1 ++ ) { tMinusDelay = ( ( _rtP -> P_4 > 0.0 ) ? _rtP -> P_4
: 0.0 ) ; tMinusDelay = simTime - tMinusDelay ; y0 [ i1 ] =
RobotPRR4deb_acc_rt_TDelayInterpolate ( tMinusDelay , 0.0 , * tBuffer , *
uBuffer , iw_CircularBufSize [ i1 ] , & iw_Last [ i1 ] , iw_Tail [ i1 ] ,
iw_Head [ i1 ] , p_P_5 [ i1 ] , 0 , ( boolean_T ) ( ssIsMinorTimeStep ( S )
&& ( ssGetTimeOfLastOutput ( S ) == ssGetT ( S ) ) ) ) ; tBuffer ++ ; uBuffer
++ ; } } } ssCallAccelRunBlock ( S , 2 , 5 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 6 , SS_CALL_MDL_OUTPUTS ) ; ssCallAccelRunBlock
( S , 2 , 7 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> nbjh4a2wkb [ 0 ] = _rtB ->
layys5wvhk [ 0 ] - _rtB -> mtmulhmo2u [ 0 ] ; _rtB -> nbjh4a2wkb [ 1 ] = _rtB
-> layys5wvhk [ 1 ] - _rtB -> mtmulhmo2u [ 1 ] ; _rtB -> nbjh4a2wkb [ 2 ] =
_rtB -> layys5wvhk [ 2 ] - _rtB -> mtmulhmo2u [ 2 ] ; _rtB -> nbjh4a2wkb [ 3
] = _rtB -> layys5wvhk [ 3 ] - _rtB -> mtmulhmo2u [ 3 ] ; h00ezikeyj_idx_0 =
_rtP -> P_6 * _rtB -> nbjh4a2wkb [ 0 ] ; h00ezikeyj_idx_1 = _rtP -> P_6 *
_rtB -> nbjh4a2wkb [ 1 ] ; h00ezikeyj_idx_2 = _rtP -> P_6 * _rtB ->
nbjh4a2wkb [ 2 ] ; h00ezikeyj_idx_3 = _rtP -> P_6 * _rtB -> nbjh4a2wkb [ 3 ]
; ssCallAccelRunBlock ( S , 2 , 10 , SS_CALL_MDL_OUTPUTS ) ; _rtB ->
fve1ezraz5 [ 0 ] = ( _rtB -> layys5wvhk [ 0 ] - _rtB -> mql0uu2ddi [ 0 ] ) *
_rtP -> P_7 + h00ezikeyj_idx_0 ; _rtB -> fve1ezraz5 [ 1 ] = ( _rtB ->
layys5wvhk [ 1 ] - _rtB -> mql0uu2ddi [ 1 ] ) * _rtP -> P_7 +
h00ezikeyj_idx_1 ; _rtB -> fve1ezraz5 [ 2 ] = ( _rtB -> layys5wvhk [ 2 ] -
_rtB -> mql0uu2ddi [ 2 ] ) * _rtP -> P_7 + h00ezikeyj_idx_2 ; _rtB ->
fve1ezraz5 [ 3 ] = ( _rtB -> layys5wvhk [ 3 ] - _rtB -> mql0uu2ddi [ 3 ] ) *
_rtP -> P_7 + h00ezikeyj_idx_3 ; _rtB -> mdmeqplsik [ 0 ] = _rtP -> P_8 [ 0 ]
; _rtB -> mdmeqplsik [ 1 ] = _rtP -> P_8 [ 1 ] ; _rtB -> mdmeqplsik [ 2 ] =
_rtP -> P_8 [ 2 ] ; _rtB -> mdmeqplsik [ 3 ] = _rtP -> P_8 [ 3 ] ;
ssCallAccelRunBlock ( S , 0 , 0 , SS_CALL_MDL_OUTPUTS ) ; ssCallAccelRunBlock
( S , 1 , 0 , SS_CALL_MDL_OUTPUTS ) ; for ( i = 0 ; i < 6 ; i ++ ) { _rtB ->
l2qlxafopz [ i ] = _rtP -> P_10 [ i ] * _rtP -> P_9 ; } jx5q0qvnjp = ssGetT (
S ) ; _rtB -> aisxq4lwwk = _rtP -> P_11 ; _rtB -> ete2axxtwo = jx5q0qvnjp +
_rtB -> aisxq4lwwk ; ssCallAccelRunBlock ( S , 2 , 22 , SS_CALL_MDL_OUTPUTS )
; _rtB -> h2bg4jirut [ 0 ] = _rtP -> P_13 [ 0 ] ; _rtB -> h2bg4jirut [ 1 ] =
_rtP -> P_13 [ 1 ] ; _rtB -> h2bg4jirut [ 2 ] = _rtP -> P_13 [ 2 ] ; _rtB ->
h2bg4jirut [ 3 ] = _rtP -> P_13 [ 3 ] ; for ( i = 0 ; i < 6 ; i ++ ) { _rtB
-> ennvnxnrlj [ i ] = _rtP -> P_12 [ i ] ; _rtB -> l3qcvl2zr1 [ i ] = _rtB ->
c5znxqafno [ i + 8 ] + _rtB -> ennvnxnrlj [ i ] ; } _rtB -> o0abxadlac [ 0 ]
= _rtB -> c5znxqafno [ 0 ] + _rtB -> h2bg4jirut [ 0 ] ; _rtB -> o0abxadlac [
1 ] = _rtB -> c5znxqafno [ 1 ] + _rtB -> h2bg4jirut [ 1 ] ; _rtB ->
o0abxadlac [ 2 ] = _rtB -> c5znxqafno [ 2 ] + _rtB -> h2bg4jirut [ 2 ] ; _rtB
-> o0abxadlac [ 3 ] = _rtB -> c5znxqafno [ 3 ] + _rtB -> h2bg4jirut [ 3 ] ;
ssCallAccelRunBlock ( S , 2 , 27 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 28 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 29 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 30 , SS_CALL_MDL_OUTPUTS ) ; _rtB -> d3aqkyzusi
[ 0 ] = _rtB -> du0zzedao5 [ 0 ] - _rtB -> mv4jwi0p02 [ 0 ] ; _rtB ->
d3aqkyzusi [ 1 ] = _rtB -> du0zzedao5 [ 1 ] - _rtB -> mv4jwi0p02 [ 1 ] ; _rtB
-> d3aqkyzusi [ 2 ] = _rtB -> du0zzedao5 [ 2 ] - _rtB -> mv4jwi0p02 [ 5 ] ;
ssCallAccelRunBlock ( S , 2 , 32 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 33 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 34 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 35 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 36 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 37 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 38 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 39 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 40 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 41 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 42 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 43 , SS_CALL_MDL_OUTPUTS ) ;
ssCallAccelRunBlock ( S , 2 , 44 , SS_CALL_MDL_OUTPUTS ) ; UNUSED_PARAMETER (
tid ) ; }
#define MDL_UPDATE
static void mdlUpdate ( SimStruct * S , int_T tid ) { cuhhlll434 * _rtB ;
kdzsm2juuu * _rtP ; lwbmduhwor * _rtDW ; _rtDW = ( ( lwbmduhwor * )
ssGetRootDWork ( S ) ) ; _rtP = ( ( kdzsm2juuu * ) ssGetDefaultParam ( S ) )
; _rtB = ( ( cuhhlll434 * ) _ssGetBlockIO ( S ) ) ; { real_T * * uBuffer = (
real_T * * ) & _rtDW -> g5hbf24wxm . TUbufferPtrs [ 0 ] ; real_T * * tBuffer
= ( real_T * * ) & _rtDW -> g5hbf24wxm . TUbufferPtrs [ 6 ] ; real_T simTime
= ssGetT ( S ) ; { int_T i1 ; const real_T * u0 = & _rtB -> l3qcvl2zr1 [ 0 ]
; int_T * iw_Tail = & _rtDW -> cb55khv33w . Tail [ 0 ] ; int_T * iw_Head = &
_rtDW -> cb55khv33w . Head [ 0 ] ; int_T * iw_Last = & _rtDW -> cb55khv33w .
Last [ 0 ] ; int_T * iw_CircularBufSize = & _rtDW -> cb55khv33w .
CircularBufSize [ 0 ] ; for ( i1 = 0 ; i1 < 6 ; i1 ++ ) { iw_Head [ i1 ] = (
( iw_Head [ i1 ] < ( iw_CircularBufSize [ i1 ] - 1 ) ) ? ( iw_Head [ i1 ] + 1
) : 0 ) ; if ( iw_Head [ i1 ] == iw_Tail [ i1 ] ) { if ( !
RobotPRR4deb_acc_rt_TDelayUpdateTailOrGrowBuf ( & iw_CircularBufSize [ i1 ] ,
& iw_Tail [ i1 ] , & iw_Head [ i1 ] , & iw_Last [ i1 ] , simTime - _rtP ->
P_0 , tBuffer , uBuffer , ( NULL ) , ( boolean_T ) 0 , false , & _rtDW ->
cb55khv33w . MaxNewBufSize ) ) { ssSetErrorStatus ( S ,
"tdelay memory allocation error" ) ; return ; } } ( * tBuffer ++ ) [ iw_Head
[ i1 ] ] = simTime ; ( * uBuffer ++ ) [ iw_Head [ i1 ] ] = u0 [ i1 ] ; } } }
{ real_T * * uBuffer = ( real_T * * ) & _rtDW -> c1refq2331 . TUbufferPtrs [
0 ] ; real_T * * tBuffer = ( real_T * * ) & _rtDW -> c1refq2331 .
TUbufferPtrs [ 4 ] ; real_T simTime = ssGetT ( S ) ; _rtDW -> mkfvho51fk .
Head [ 0 ] = ( ( _rtDW -> mkfvho51fk . Head [ 0 ] < ( _rtDW -> mkfvho51fk .
CircularBufSize [ 0 ] - 1 ) ) ? ( _rtDW -> mkfvho51fk . Head [ 0 ] + 1 ) : 0
) ; if ( _rtDW -> mkfvho51fk . Head [ 0 ] == _rtDW -> mkfvho51fk . Tail [ 0 ]
) { if ( ! RobotPRR4deb_acc_rt_TDelayUpdateTailOrGrowBuf ( & _rtDW ->
mkfvho51fk . CircularBufSize [ 0 ] , & _rtDW -> mkfvho51fk . Tail [ 0 ] , &
_rtDW -> mkfvho51fk . Head [ 0 ] , & _rtDW -> mkfvho51fk . Last [ 0 ] ,
simTime - _rtP -> P_2 , tBuffer , uBuffer , ( NULL ) , ( boolean_T ) 0 ,
false , & _rtDW -> mkfvho51fk . MaxNewBufSize ) ) { ssSetErrorStatus ( S ,
"tdelay memory allocation error" ) ; return ; } } ( * tBuffer ++ ) [ _rtDW ->
mkfvho51fk . Head [ 0 ] ] = simTime ; ( * uBuffer ++ ) [ _rtDW -> mkfvho51fk
. Head [ 0 ] ] = _rtB -> o0abxadlac [ 0 ] ; _rtDW -> mkfvho51fk . Head [ 1 ]
= ( ( _rtDW -> mkfvho51fk . Head [ 1 ] < ( _rtDW -> mkfvho51fk .
CircularBufSize [ 1 ] - 1 ) ) ? ( _rtDW -> mkfvho51fk . Head [ 1 ] + 1 ) : 0
) ; if ( _rtDW -> mkfvho51fk . Head [ 1 ] == _rtDW -> mkfvho51fk . Tail [ 1 ]
) { if ( ! RobotPRR4deb_acc_rt_TDelayUpdateTailOrGrowBuf ( & _rtDW ->
mkfvho51fk . CircularBufSize [ 1 ] , & _rtDW -> mkfvho51fk . Tail [ 1 ] , &
_rtDW -> mkfvho51fk . Head [ 1 ] , & _rtDW -> mkfvho51fk . Last [ 1 ] ,
simTime - _rtP -> P_2 , tBuffer , uBuffer , ( NULL ) , ( boolean_T ) 0 ,
false , & _rtDW -> mkfvho51fk . MaxNewBufSize ) ) { ssSetErrorStatus ( S ,
"tdelay memory allocation error" ) ; return ; } } ( * tBuffer ++ ) [ _rtDW ->
mkfvho51fk . Head [ 1 ] ] = simTime ; ( * uBuffer ++ ) [ _rtDW -> mkfvho51fk
. Head [ 1 ] ] = _rtB -> o0abxadlac [ 1 ] ; _rtDW -> mkfvho51fk . Head [ 2 ]
= ( ( _rtDW -> mkfvho51fk . Head [ 2 ] < ( _rtDW -> mkfvho51fk .
CircularBufSize [ 2 ] - 1 ) ) ? ( _rtDW -> mkfvho51fk . Head [ 2 ] + 1 ) : 0
) ; if ( _rtDW -> mkfvho51fk . Head [ 2 ] == _rtDW -> mkfvho51fk . Tail [ 2 ]
) { if ( ! RobotPRR4deb_acc_rt_TDelayUpdateTailOrGrowBuf ( & _rtDW ->
mkfvho51fk . CircularBufSize [ 2 ] , & _rtDW -> mkfvho51fk . Tail [ 2 ] , &
_rtDW -> mkfvho51fk . Head [ 2 ] , & _rtDW -> mkfvho51fk . Last [ 2 ] ,
simTime - _rtP -> P_2 , tBuffer , uBuffer , ( NULL ) , ( boolean_T ) 0 ,
false , & _rtDW -> mkfvho51fk . MaxNewBufSize ) ) { ssSetErrorStatus ( S ,
"tdelay memory allocation error" ) ; return ; } } ( * tBuffer ++ ) [ _rtDW ->
mkfvho51fk . Head [ 2 ] ] = simTime ; ( * uBuffer ++ ) [ _rtDW -> mkfvho51fk
. Head [ 2 ] ] = _rtB -> o0abxadlac [ 2 ] ; _rtDW -> mkfvho51fk . Head [ 3 ]
= ( ( _rtDW -> mkfvho51fk . Head [ 3 ] < ( _rtDW -> mkfvho51fk .
CircularBufSize [ 3 ] - 1 ) ) ? ( _rtDW -> mkfvho51fk . Head [ 3 ] + 1 ) : 0
) ; if ( _rtDW -> mkfvho51fk . Head [ 3 ] == _rtDW -> mkfvho51fk . Tail [ 3 ]
) { if ( ! RobotPRR4deb_acc_rt_TDelayUpdateTailOrGrowBuf ( & _rtDW ->
mkfvho51fk . CircularBufSize [ 3 ] , & _rtDW -> mkfvho51fk . Tail [ 3 ] , &
_rtDW -> mkfvho51fk . Head [ 3 ] , & _rtDW -> mkfvho51fk . Last [ 3 ] ,
simTime - _rtP -> P_2 , tBuffer , uBuffer , ( NULL ) , ( boolean_T ) 0 ,
false , & _rtDW -> mkfvho51fk . MaxNewBufSize ) ) { ssSetErrorStatus ( S ,
"tdelay memory allocation error" ) ; return ; } } ( * tBuffer ) [ _rtDW ->
mkfvho51fk . Head [ 3 ] ] = simTime ; ( * uBuffer ) [ _rtDW -> mkfvho51fk .
Head [ 3 ] ] = _rtB -> o0abxadlac [ 3 ] ; } { real_T * * uBuffer = ( real_T *
* ) & _rtDW -> g5n0vcnuuu . TUbufferPtrs [ 0 ] ; real_T * * tBuffer = (
real_T * * ) & _rtDW -> g5n0vcnuuu . TUbufferPtrs [ 4 ] ; real_T simTime =
ssGetT ( S ) ; _rtDW -> mzedfxafby . Head [ 0 ] = ( ( _rtDW -> mzedfxafby .
Head [ 0 ] < ( _rtDW -> mzedfxafby . CircularBufSize [ 0 ] - 1 ) ) ? ( _rtDW
-> mzedfxafby . Head [ 0 ] + 1 ) : 0 ) ; if ( _rtDW -> mzedfxafby . Head [ 0
] == _rtDW -> mzedfxafby . Tail [ 0 ] ) { if ( !
RobotPRR4deb_acc_rt_TDelayUpdateTailOrGrowBuf ( & _rtDW -> mzedfxafby .
CircularBufSize [ 0 ] , & _rtDW -> mzedfxafby . Tail [ 0 ] , & _rtDW ->
mzedfxafby . Head [ 0 ] , & _rtDW -> mzedfxafby . Last [ 0 ] , simTime - _rtP
-> P_4 , tBuffer , uBuffer , ( NULL ) , ( boolean_T ) 0 , false , & _rtDW ->
mzedfxafby . MaxNewBufSize ) ) { ssSetErrorStatus ( S ,
"tdelay memory allocation error" ) ; return ; } } ( * tBuffer ++ ) [ _rtDW ->
mzedfxafby . Head [ 0 ] ] = simTime ; ( * uBuffer ++ ) [ _rtDW -> mzedfxafby
. Head [ 0 ] ] = _rtB -> c5znxqafno [ 4 ] ; _rtDW -> mzedfxafby . Head [ 1 ]
= ( ( _rtDW -> mzedfxafby . Head [ 1 ] < ( _rtDW -> mzedfxafby .
CircularBufSize [ 1 ] - 1 ) ) ? ( _rtDW -> mzedfxafby . Head [ 1 ] + 1 ) : 0
) ; if ( _rtDW -> mzedfxafby . Head [ 1 ] == _rtDW -> mzedfxafby . Tail [ 1 ]
) { if ( ! RobotPRR4deb_acc_rt_TDelayUpdateTailOrGrowBuf ( & _rtDW ->
mzedfxafby . CircularBufSize [ 1 ] , & _rtDW -> mzedfxafby . Tail [ 1 ] , &
_rtDW -> mzedfxafby . Head [ 1 ] , & _rtDW -> mzedfxafby . Last [ 1 ] ,
simTime - _rtP -> P_4 , tBuffer , uBuffer , ( NULL ) , ( boolean_T ) 0 ,
false , & _rtDW -> mzedfxafby . MaxNewBufSize ) ) { ssSetErrorStatus ( S ,
"tdelay memory allocation error" ) ; return ; } } ( * tBuffer ++ ) [ _rtDW ->
mzedfxafby . Head [ 1 ] ] = simTime ; ( * uBuffer ++ ) [ _rtDW -> mzedfxafby
. Head [ 1 ] ] = _rtB -> c5znxqafno [ 5 ] ; _rtDW -> mzedfxafby . Head [ 2 ]
= ( ( _rtDW -> mzedfxafby . Head [ 2 ] < ( _rtDW -> mzedfxafby .
CircularBufSize [ 2 ] - 1 ) ) ? ( _rtDW -> mzedfxafby . Head [ 2 ] + 1 ) : 0
) ; if ( _rtDW -> mzedfxafby . Head [ 2 ] == _rtDW -> mzedfxafby . Tail [ 2 ]
) { if ( ! RobotPRR4deb_acc_rt_TDelayUpdateTailOrGrowBuf ( & _rtDW ->
mzedfxafby . CircularBufSize [ 2 ] , & _rtDW -> mzedfxafby . Tail [ 2 ] , &
_rtDW -> mzedfxafby . Head [ 2 ] , & _rtDW -> mzedfxafby . Last [ 2 ] ,
simTime - _rtP -> P_4 , tBuffer , uBuffer , ( NULL ) , ( boolean_T ) 0 ,
false , & _rtDW -> mzedfxafby . MaxNewBufSize ) ) { ssSetErrorStatus ( S ,
"tdelay memory allocation error" ) ; return ; } } ( * tBuffer ++ ) [ _rtDW ->
mzedfxafby . Head [ 2 ] ] = simTime ; ( * uBuffer ++ ) [ _rtDW -> mzedfxafby
. Head [ 2 ] ] = _rtB -> c5znxqafno [ 6 ] ; _rtDW -> mzedfxafby . Head [ 3 ]
= ( ( _rtDW -> mzedfxafby . Head [ 3 ] < ( _rtDW -> mzedfxafby .
CircularBufSize [ 3 ] - 1 ) ) ? ( _rtDW -> mzedfxafby . Head [ 3 ] + 1 ) : 0
) ; if ( _rtDW -> mzedfxafby . Head [ 3 ] == _rtDW -> mzedfxafby . Tail [ 3 ]
) { if ( ! RobotPRR4deb_acc_rt_TDelayUpdateTailOrGrowBuf ( & _rtDW ->
mzedfxafby . CircularBufSize [ 3 ] , & _rtDW -> mzedfxafby . Tail [ 3 ] , &
_rtDW -> mzedfxafby . Head [ 3 ] , & _rtDW -> mzedfxafby . Last [ 3 ] ,
simTime - _rtP -> P_4 , tBuffer , uBuffer , ( NULL ) , ( boolean_T ) 0 ,
false , & _rtDW -> mzedfxafby . MaxNewBufSize ) ) { ssSetErrorStatus ( S ,
"tdelay memory allocation error" ) ; return ; } } ( * tBuffer ) [ _rtDW ->
mzedfxafby . Head [ 3 ] ] = simTime ; ( * uBuffer ) [ _rtDW -> mzedfxafby .
Head [ 3 ] ] = _rtB -> c5znxqafno [ 7 ] ; } UNUSED_PARAMETER ( tid ) ; }
static void mdlInitializeSizes ( SimStruct * S ) { ssSetChecksumVal ( S , 0 ,
1919388131U ) ; ssSetChecksumVal ( S , 1 , 246410022U ) ; ssSetChecksumVal (
S , 2 , 2761584005U ) ; ssSetChecksumVal ( S , 3 , 528639220U ) ; { mxArray *
slVerStructMat = NULL ; mxArray * slStrMat = mxCreateString ( "simulink" ) ;
char slVerChar [ 10 ] ; int status = mexCallMATLAB ( 1 , & slVerStructMat , 1
, & slStrMat , "ver" ) ; if ( status == 0 ) { mxArray * slVerMat = mxGetField
( slVerStructMat , 0 , "Version" ) ; if ( slVerMat == NULL ) { status = 1 ; }
else { status = mxGetString ( slVerMat , slVerChar , 10 ) ; } }
mxDestroyArray ( slStrMat ) ; mxDestroyArray ( slVerStructMat ) ; if ( (
status == 1 ) || ( strcmp ( slVerChar , "8.5" ) != 0 ) ) { return ; } }
ssSetOptions ( S , SS_OPTION_EXCEPTION_FREE_CODE ) ; if ( ssGetSizeofDWork (
S ) != sizeof ( lwbmduhwor ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal DWork sizes do "
"not match for accelerator mex file." ) ; } if ( ssGetSizeofGlobalBlockIO ( S
) != sizeof ( cuhhlll434 ) ) { ssSetErrorStatus ( S ,
"Unexpected error: Internal BlockIO sizes do "
"not match for accelerator mex file." ) ; } { int ssSizeofParams ;
ssGetSizeofParams ( S , & ssSizeofParams ) ; if ( ssSizeofParams != sizeof (
kdzsm2juuu ) ) { static char msg [ 256 ] ; sprintf ( msg ,
"Unexpected error: Internal Parameters sizes do "
"not match for accelerator mex file." ) ; } } _ssSetDefaultParam ( S , (
real_T * ) & bzsanxx3ft ) ; rt_InitInfAndNaN ( sizeof ( real_T ) ) ; } static
void mdlInitializeSampleTimes ( SimStruct * S ) { { SimStruct * childS ;
SysOutputFcn * callSysFcns ; childS = ssGetSFunction ( S , 0 ) ; callSysFcns
= ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; childS = ssGetSFunction ( S , 1 ) ; callSysFcns =
ssGetCallSystemOutputFcnList ( childS ) ; callSysFcns [ 3 + 0 ] = (
SysOutputFcn ) ( NULL ) ; } } static void mdlTerminate ( SimStruct * S ) { }
#include "simulink.c"
