% Main script used to interface MATLAB and ANSYS
clear all

consts;
[tf,Time,Xd,dXd,ddXd,qd,dqd]= Calcultraj(J,Ap,Vp,X0,XA,min(0.001,dt),dg);

Fc= InvDyn(Xd,dXd,ddXd,qd,dqd,PMass,dg);

delete('Interface/*.txt', 'ANSYS/Workspace/*');
t_end=tf+t_stab;
%init;
%t = t_start:dt:t_end;



%sim(model_name, t);
