function init()
  % .initialization of robot parameters
  
  global init_filename
  write_variables(init_filename);
  system('SET KMP_STACKSIZE=2048M & "C:\Program Files\ANSYS Inc\v150\ANSYS\bin\winx64\ansys150.exe"  -p ane3fl -np 4 -dir "C:\Users\Julien\Documents\These\Robot PRR4\ANSYS\Workspace" -j "RobotPRR4" -b -i "C:\Users\Julien\Documents\These\Robot PRR4\ANSYS\Fichier script\InitROB.txt" -o "C:\Users\Julien\Documents\These\Robot PRR4\ANSYS\Workspace\file.out"');
  %system('SET KMP_STACKSIZE=2048M & "C:\Program Files\ANSYS Inc\v162\ANSYS\bin\winx64\ansys162.exe"  -p ane3fl -np 4 -dir "C:\Users\Julien\Documents\These\Robot PRR4\ANSYS\Workspace" -j "RobotPRR4" -b -i "C:\Users\Julien\Documents\These\Robot PRR4\ANSYS\Fichier script\InitROB.txt" -o "C:\Users\Julien\Documents\These\Robot PRR4\ANSYS\Workspace\file.out"');
end


function write_variables(filename)
    consts;
    % Calcul du MGI
    [ q, B ] = MGI(X0, dg);
    assignin('base', 'q0', q);
    %assignin('base', 'X0', X);

    % Ecriture dans le fichier de sortie
    file = fopen(filename, 'w');

    for i = 1 : length(vars)
        assignin('base', vars{i}{1}, vars{i}{2});
        fprintf(file, '*SET, %s, %6.4f\n', vars{i}{1}, vars{i}{2});
    end

    assignin('base', 'X0', X0);
    fprintf(file,'*SET, Up, %6.4f, %6.4f\n', X0(1), X0(2));

    assignin('base', 'B', B);
    for i = 1:4
        fprintf(file,'*SET, Un%d, %6.4f, %6.4f\n', i, B(i,1), B(i,2));
    end

    for i = 1:2
        fprintf(file,'*SET, Um%d, %6.4f, %6.4f\n', i, q(i,1), dg(4));
    end
    for i = 3:4
        fprintf(file,'*SET, Um%d, %6.4f, %6.4f\n', i, q(i,1), -dg(4));
    end

    fprintf(file, '*DIM, RST, ARRAY, 14\n');

    fclose(file);
end
