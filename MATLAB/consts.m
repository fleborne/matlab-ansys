global init_filename tau_filename robot_name
%%%%%%%%%%%%% User can modify these values wrt to ANSYS scripts values %%%%

% Dialog files names

%mkdir('Interface');
%addpath('Interface\');

init_filename  = 'Interface\Variables.txt';   % Input file for ANSYS (robot parameters)
tau_filename   = 'Interface\ActionRA.txt';   % Input file for ANSYS (torques)
robot_name     = 'RobotPRR4';   % Robot name
model_name     = 'RobotPRR4.slx';   % Simulink model name

dg = [ .1 ; .1 ; 1 ; 1 ];
l = sqrt((dg(3)-dg(1))^2+(dg(4)-dg(2))^2);
ymax=l-dg(3);

X0 = [ 0 ; 0*ymax ; 0 ];
XA = [ 160 ; 0*ymax ; 0 ];

J=5000;
Ap=300;
Vp=100;


t_stab=0;


       E1=400e9;
       nu1=.3;
       d1=2600;
       E2=210e9;
       nu2=.3;
       d2=7800;
       ep=.1;
       R=0.05;
       Mm=8;
       dt=0.0002;


       Massmotor=8;

mb=pi*d1*l*R^2*(1-0.9^2);
masspl=d2*4*dg(1)*dg(2)*ep;
Ipl=masspl/12*((2*dg(1))^2+(2*dg(2))^2);


Mm=Massmotor+mb/2;
mp=masspl+2*mb;
I=Ipl+2*mb*(dg(1)^2+dg(2)^2);
PMass=[mp,I,Mm];


% variables names and values
% format {'name', value}. The value can be of any type.
vars = {
        {'E1', E1}
        {'nu1', nu1}
        {'d1', d1}
        {'E2', E2}
        {'nu2', nu2}
        {'d2', d2}
        {'ep', ep}
        {'R', R}
        {'Mm', Mm}
};






